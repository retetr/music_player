#!/usr/bin/python

# Monitor and autoplay on bluetooth connection

import os
import sys
import subprocess
import time

def blue_it():
	status = subprocess.call('ls /dev/input/event0 2>/dev/null', shell=True)

	if status == 0:
		print('Starting song in 3')
		time.sleep(3)
		subprocess.Popen(
			'mpg321 -l 0 /home/pi/Music/just_a_closer_walk_with_thee.mp3', 
			shell=True)

	while status == 0:
		print("BT Up")
		print(status)
		time.sleep(5)

		status = subprocess.call('ls /dev/input/event0 2>/dev/null', shell=True)

	waiting()

def waiting():
	subprocess.call('killall -9 mpg321', shell=True)
	time.sleep(3)
	status = subprocess.call('ls /dev/input/event0 2>/dev/null', shell=True)

	while status == 2:
		print("BT Down")
		time.sleep(5)
		status = subprocess.call('ls /dev/input/event0 2>/dev/null', shell=True)
	else:
		blue_it()

blue_it()
